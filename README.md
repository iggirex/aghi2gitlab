We'll create projects and grant one another roles in them.

We'll practice the process of creating an Issue, Branch, and Merge Request.

We'll practice updating and committing changes to code.
dbjskbbdk
We'll practice code reviews and merges.

We'll play with Pages and GitLab CI.

Finally, we'll shake a stick at all the GitLab awesomeness we didn't touch, and discuss [GitLab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)

We will end the four-hour session duly impressed by GitLab's comprehensive capabilities and high level of integration between important services.

Materials Home: https://gitlab.com/sofreeus/aghi2gitlab

LABS/
- [Module 0](LABS/Overview-and-setup.md)
- [Issues I2P 1/2](LABS/Issues-templates-merges.md)
- [CICD I2P 2/2](LABS/CI-and-pages.md)
- [Beyond](LABS/Other-Things.md)
- [BYO](LABS/Build-Your-Own-WIP.md)(WIP)
