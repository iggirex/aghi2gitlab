# Pages and CI

## Talk

The `.gitlab-ci.yml` file controls the action.

`Pages` is a magic job.

- CI/CD *is* DevOps
- Stages have environments
- Dev environments can/should be throwaway
- Acceptance environments should be prod-like

## Exercises

1. Fork this project on GitLab.com
*  Note that CI jobs start failing
*  Revert to the simple Pages CI
*  Make a change to the Pages script and observe the change to the GitLab Pages

---

1. Restore the staged CI, populate the variables, make a change to the docs in site.md/ and test the deployment jobs.

---

Notes:

*build* should produce an *artifact* of some sort, a package (rpm, deb, tar, etc.) or a disk image or a container image
- build, test, store artifact
- build once, deploy and test many times

Stages have Environments. I recommend a four-stage approach.
- Development
- Testing (bots test)
- Acceptance (humans test)
- Production (customers)

This project sets the URL like this:
- For all but Production, the URL is https://$ENVIRONMENT-$MACGUFFIN.sofree.us
- The production URL is https://$MACGUFFIN.sofree.us

[CI_PAGES_URL](https://sofreeus.gitlab.io/aghi2gitlab)
