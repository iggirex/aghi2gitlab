Anyone may submit Issues or Merge Requests

The whole goal of AGHI2GitLab is to show that GitLab does everything needed and much that is wanted, that it's tightly integrated, and freer, in both libre and gratis senses, than other git servers (like GitHub) and suites (like Atlassian).
